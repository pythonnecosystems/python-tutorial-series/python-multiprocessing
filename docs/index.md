# Python 멀티프로세싱: 프로세스, 풀, 큐 <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 multiprocessing 모듈을 사용하여 병렬 실행을 위한 프로세스를 생성하고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./multiprocessing.md#intro)
1. [멀티프로세싱이란?](./multiprocessing.md#sec_02)
1. [프로세스 생성과 실행 방법](./multiprocessing.md#sec_03)
1. [프로세스 통신과 동기화 방법](./multiprocessing.md#sec_04)
1. [병렬 실행을 위한 풀과 맵 사용 방법](./multiprocessing.md#sec_05)
1. [프로세스간 통신을 위한 큐 사용 방법](./multiprocessing.md#sec_06)
1. [예외 처리와 프로세스 종료 방법](./multiprocessing.md#sec_07)
1. [요약](./multiprocessing.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 42 — Python Multiprocessing: Process, Pool, Queue](https://python.plainenglish.io/python-tutorial-42-python-multiprocessing-process-pool-queue-4e59b7f01023?sk=294e8427b178e1b64f0dd215479729ac)를 편역하였습니다.
